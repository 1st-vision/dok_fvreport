.. |label| replace:: Kundenauswertungen
.. |snippet| replace:: FvReports
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.3.0
.. |maxVersion| replace:: 5.3.4
.. |version| replace:: 1.0.1
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Ermöglicht die Pflege von Auswertungsabfragen im Backend unter dem Kunden-Menü.
Ermöglicht Kunden die Erstellung und den Download von Auswertungen im Kundenkonto.


Frontend
--------
Nach Installation und Aktivierung des Plugins erscheint ein neuer Menüpunkt "Auswertungen" im Shopware Kundenkonto.

.. image:: FvReports3.png

Bei Aufruf gelangt man auf den Frontend-Plugin-Controller "Reports". nachdem im Backend eine Auswertung freigegeben wurde, kann der Kunde sich jederzeit eine aktuelle Auswertung generieren lassen und sie dann anschließend herunterladen.

.. image:: FvReports4.png
.. image:: FvReports5.png

Je nachdem wie es im backend hinterlegt ist, können max 3 Werte als variable übergeben werden, diese kann der Kunde dann mit definieren wie zB. die Artikelnummer.



Backend
-------
.. image:: FvReports1.png

Sie gelangen über den Menü-Punkt Kunden zu dem Punkt "Auswertungen für Kunden".

Konfiguration
_____________
.. image:: FvReports2.png

Hier können Sie die Auswertung konfigurieren.
:Name: Dies ist der Name der Auswertung wie Sie dem Kunden angezeigt werden soll.
:Aktiv?: Hier können Sie die Auswertung aktivieren und deaktivieren, wenn sie aktiviert ist wird sie im frontend angezeigt.
:Query: Hier geben Sie die Query ein.
:$var1: Dies ist eine variable die Sie benutzen können. Wenn Sie sie nicht benutzen wollen wählen Sie "nicht verwenden".
:$var2: Dies ist eine variable die Sie benutzen können. Wenn Sie sie nicht benutzen wollen wählen Sie "nicht verwenden".
:$var3: Dies ist eine variable die Sie benutzen können. Wenn Sie sie nicht benutzen wollen wählen Sie "nicht verwenden".



Beispiel
________

SELECT su.customernumber as Kundennummer , o.ordernumber as Bestellnummer, o.invoice_amount_net as GesamtNetto, o.ordertime as  Bestelldatum, 
CONCAT(os.Company,', ',os.street,', ',os.zipcode,' ',os.city) AS Lieferanschrift,
d.articleordernumber as Artikelnummer, d.price as Preis, d.quantity as Menge, d.name as Artikelbezeichnung, d.ean as EANNummer
FROM s_order o
INNER JOIN s_order_details d
on d.ordernumber = o.ordernumber
INNER JOIN s_order_attributes oa
ON oa.orderid = o.id
INNER JOIN s_user su
ON su.id = o.userid
INNER JOIN s_order_shippingaddress os
ON os.orderid = o.id
WHERE o.userID = $customerId
AND o.ordernumber <> 0
AND (d.articleordernumber = $var1 OR '' = $var1)
AND 
(o.ordertime > SUBSTRING($var2, 0, 10) AND o.ordertime < SUBSTRING($var2, 15, 10) OR '' = $var2)

$var1 = String
$var2 = DataRange
$var3 = nicht verwenden


technische Beschreibung
------------------------
Als variablen können Sie folgende Werte angeben:

:String:
:Date:
:DateTime:
:DateRange:


Modifizierte Template-Dateien
-----------------------------
:/account/sidebar.tpl:



